import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppService {

  constructor(private readonly configService: ConfigService) { }

  getHealth() {
    return {
      name: this.configService.get<string>('APP_NAME'),
      port: this.configService.get<string>('APP_NAME'),
      version: this.configService.get<string>('VERSION')
    }
  }
}
