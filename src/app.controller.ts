import { Controller, Request, Get, Post, UseGuards, Headers } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { SkipAuth } from './auth/decorators/auth.decorator';
import { LocalAuthGuard } from './auth/guards/local-auth.guard';
import { SkipThrottle } from '@nestjs/throttler/dist/throttler.decorator';

@Controller()
export class AppController {

  constructor(
    private readonly appService: AppService,
    private authService: AuthService
  ) { }

  @SkipAuth()
  @SkipThrottle()
  @Get()
  get() {
    return this.appService.getHealth();
  }

  @UseGuards(LocalAuthGuard)
  @SkipAuth()
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user[0]);
  }

  @Get('decoded')
  @SkipAuth()
  @SkipThrottle()
  getProfile(@Headers('authorization') auth) {
    return this.authService.getDecoded(auth);
  }
}
