import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateTodoDto } from './dto/create-task.dto';
import { Todo } from './schemas/todo.schema';
import * as mongoose from 'mongoose';
import { Status } from './enums/status.enums';
import { AuthService } from 'src/auth/auth.service';

function ObjectId(_id: string): mongoose.Types.ObjectId {
    return new mongoose.Types.ObjectId(_id);

}

@Injectable()
export class TodoService {
    constructor(
        @InjectModel(Todo.name)
        private readonly todoModel: mongoose.Model<Todo>,
        private authService: AuthService
    ) { }

    getAll(): Promise<Todo[]> {
        const filter = { status: { $ne: Status.DELETED } };
        return this.todoModel.find(filter).exec();
    }

    async create(createTodoDto: CreateTodoDto, auth: string): Promise<Todo> {
        let user = this.authService.getDecoded(auth);
        createTodoDto.createdById = user['username'];
        createTodoDto.createdByName = user['name'];
        createTodoDto.updatedById = user['username'];
        createTodoDto.updatedByName = user['name'];
        const createdTask = new this.todoModel(createTodoDto);
        return createdTask.save();
    }

    async put(id: string, createTodoDto: CreateTodoDto, auth: string): Promise<Todo> {
        let user = this.authService.getDecoded(auth);
        const filter = {
            _id: ObjectId(id),
            "status": {
                $ne: Status.DELETED
            }
        };
        const update = {
            $set: {
                task: createTodoDto.task,
                status: createTodoDto.status,
                updatedAt: new Date().toISOString(),
                updatedById: user['username'],
                updatedByName: user['name']
            }
        };
        const options = { new: true }
        return await this.todoModel.findOneAndUpdate(filter, update, options).exec();
    }

    async patch(id: string, auth: string): Promise<Todo> {
        let user = this.authService.getDecoded(auth);
        const filter = { _id: ObjectId(id) };
        const update = {
            $set: {
                status: Status.DELETED,
                updatedAt: new Date().toISOString(),
                updatedById: user['username'],
                updatedByName: user['name']
            }
        };
        const options = { new: true }
        return await this.todoModel.findOneAndUpdate(filter, update, options).exec();
    }
}
