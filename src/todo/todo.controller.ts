import { Controller, Headers, Body, Param, Get, Patch, Post, Put } from '@nestjs/common';
import { TodoService } from './todo.service';
import { CreateTodoDto } from './dto/create-task.dto';
import { Todo } from './schemas/todo.schema';

@Controller('todo')
export class TodoController {
    constructor(private readonly todoService: TodoService) { }

    @Get()
    get(): Promise<Todo[]> {
        return this.todoService.getAll();
    }

    @Post()
    post(@Body() createTodoDto: CreateTodoDto, @Headers('authorization') auth): Promise<Todo> {
        return this.todoService.create(createTodoDto, auth);
    }

    @Put('/:id')
    async put(@Body() createTodoDto: CreateTodoDto, @Param('id') id: string, @Headers('authorization') auth): Promise<Todo> {
        return await this.todoService.put(id, createTodoDto, auth);
    }

    @Patch('/:id')
    async patch(@Param('id') id: string, @Headers('authorization') auth): Promise<Todo> {
        return await this.todoService.patch(id, auth);
    }
}
