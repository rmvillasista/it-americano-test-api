export enum Status {
    PENDING = "pending",
    COMPLETED = "completed",
    DELETED = "deleted"
}
