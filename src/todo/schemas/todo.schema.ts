import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({
    collection: 'tasks',
    timestamps: {
        createdAt: true,
        updatedAt: true
    }
})
export class Todo {
    @Prop()
    task: string;

    @Prop()
    status: string;

    @Prop()
    createdById?: string;

    @Prop()
    createdByName?: string;

    @Prop({ default: Date.now })
    createdAt?: Date;

    @Prop()
    updatedById?: string;

    @Prop()
    updatedByName?: string;

    @Prop({ default: Date.now })
    updatedAt?: Date;
}

export const TodoSchema = SchemaFactory.createForClass(Todo);
