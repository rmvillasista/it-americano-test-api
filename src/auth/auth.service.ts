import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) { }

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne(username, pass);
        const encodePassword: string = Buffer.from(user[0].password, 'base64').toString('ascii');
        if (user.length > 0 && pass === encodePassword) {
            const { ...result } = user;
            return result;
        }
        return null;
    }

    async login(user: any) {
        const payload = { username: user.username, name: user.name };
        const token = this.jwtService.sign(payload);
        return {
            access_token: token,
        };
    }

    getDecoded(auth: string) {
        if (auth && auth.split(" ")[0] === "Bearer") {
            const token = auth.split(" ")[1];
            return this.jwtService.decode(token);
        }
        return null;
    }
}
