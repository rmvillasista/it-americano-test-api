import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({
    collection: 'logins',
    timestamps: {
        createdAt: true,
        updatedAt: true
    }
})
export class User {
    @Prop()
    username: string;

    @Prop()
    password: string;

    @Prop()
    name: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
