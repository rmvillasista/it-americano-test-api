import { Injectable } from '@nestjs/common';
import { User } from './schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Injectable()
export class UsersService {
    constructor(
        @InjectModel(User.name)
        private readonly userModel: mongoose.Model<User>
    ) { }

    async findOne(username: string, password: string): Promise<User[]> {
        const filter = { username };
        return await this.userModel.find(filter).exec();
    }
}
